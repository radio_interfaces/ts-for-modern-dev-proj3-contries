import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { Counrtry, Extra, Status } from 'types';

export const loadCountryByName = createAsyncThunk<
  {data: Counrtry[]},
  string,
  { extra: Extra}
>(
  '@@details/load-country-by-name',
  (name, {extra: {client, api}}) => {
    return client.get(api.searchByCountry(name));
  }
);
export const loadNeighborsByBorder = createAsyncThunk<
  {data: Counrtry[]},
  string[],
  { extra: Extra}
>(
  '@@details/load-neighbors',
  (borders, {extra: {client, api}}) => {
    return client.get(api.filterByCode(borders));
  }
);

type DetailsType = {
  currentCountry: Counrtry | null,
  neighbors: string[],
  status: Status,
  error: string | null,
}

const initialState: DetailsType = {
  currentCountry: null,
  neighbors: [],
  status: 'idle',
  error: null,
}

const detailsSlice = createSlice({
  name: '@@details',
  initialState,
  reducers: {
    clearDetails: () => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadCountryByName.pending, (state) => {
        state.status = 'loading';
        state.error = null;
      })
      .addCase(loadCountryByName.rejected, (state, action) => {
        state.status = 'rejected';
        state.error = 'Can not load data';
      })
      .addCase(loadCountryByName.fulfilled, (state, action) => {
        state.status = 'idle';
        state.currentCountry = action.payload.data[0];
      })
      .addCase(loadNeighborsByBorder.fulfilled, (state, action) => {
        state.neighbors = action.payload.data.map(country => country.name);
      })
  }
});

export const {clearDetails} = detailsSlice.actions;
export const detailsReducer = detailsSlice.reducer;
